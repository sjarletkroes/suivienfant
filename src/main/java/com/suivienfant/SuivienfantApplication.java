package com.suivienfant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SuivienfantApplication {

	public static void main(String[] args) {
		SpringApplication.run(SuivienfantApplication.class, args);
	}

}
