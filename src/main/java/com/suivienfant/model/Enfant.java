package com.suivienfant.model;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 * Objet contenant les données de l'enfant (nom, prénom, date de naissance)
 * et une liste de mots qui lui sont associés
 */
@Entity
@Table(name = "enfant")
public class Enfant {

	// -------------------------------------------------------------- Attributs.

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "nom", nullable = false)
	private String nom;
	
	@Column(name = "prenom", nullable = false)
	private String prenom;

	@Temporal(TemporalType.DATE)
	@Column(name = "date_naissance")
	private Date dateNaissance;

	@OneToMany(mappedBy = "enfant", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonManagedReference("Enfant-Mot")
	private List<Mot> listeMots;

	// ---------------------------------------------------------- Constructeurs.

	public Enfant() {

	}

	public Enfant(String nom, String prenom, Date dateNaissance) {
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaissance = dateNaissance;
	}

	// --------------------------------------------------------------- Méthodes.

	public long getId() {
		return id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Date getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public List<Mot> getListeMots() {
		return listeMots;
	}

	public void setListeMots(List<Mot> listeMots) {
		this.listeMots = listeMots;
	}

	@Override
	public String toString() {
		return "Enfant [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", dateNaissance=" + dateNaissance.toString() + "]";
	}
}