package com.suivienfant.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.suivienfant.enums.LangueEnum;

/**
 * Objet contenant un mot, sa prononciation, la langue du mot 
 * et la date de première utilisation de ce mot par l'enfant
 */
@Entity
@Table(name = "mot")
public class Mot {

	// -------------------------------------------------------------- Attributs.

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "mot", nullable = false)
	private String mot;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "langue", nullable = false)
	private LangueEnum langue;
	
	@Column(name = "prononciation")
	private String prononciation;

	@Temporal(TemporalType.DATE)
	@Column(name = "date_ajout", nullable = false)
	private Date dateAjout;

	@JoinColumn(name = "enfant_id")
	@ManyToOne(cascade = CascadeType.REFRESH, optional = false, fetch = FetchType.LAZY)
	@JsonBackReference("Enfant-Mot")
    private Enfant enfant;

	// ---------------------------------------------------------- Constructeurs.

	public Mot() {
		super();
	}

	public Mot(LangueEnum langue, String mot, String prononciation, Date dateAjout, Enfant enfant) {
		super();
		this.langue = langue;
		this.mot = mot;
		this.prononciation = prononciation;
		this.dateAjout = dateAjout;
		this.enfant = enfant;
	}

	// --------------------------------------------------------------- Méthodes.

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMot() {
		return mot;
	}

	public void setMot(String mot) {
		this.mot = mot;
	}

	public LangueEnum getLangue() {
		return langue;
	}

	public void setLangue(LangueEnum langue) {
		this.langue = langue;
	}

	public String getPrononciation() {
		return prononciation;
	}

	public void setPrononciation(String prononciation) {
		this.prononciation = prononciation;
	}

	public Date getDateAjout() {
		return dateAjout;
	}

	public void setDateAjout(Date dateAjout) {
		this.dateAjout = dateAjout;
	}

	public Enfant getEnfant() {
		return enfant;
	}

	public void setEnfant(Enfant enfant) {
		this.enfant = enfant;
	}

}
