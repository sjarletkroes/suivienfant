package com.suivienfant.service;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.suivienfant.model.Enfant;

/**
 * Service pour la récupération des enfants
 */
public interface EnfantService extends JpaRepository<Enfant, Long> {
  
	/**
	 * Retourne les enfants ayant la chaine nom dans leur nom
	 * 
	 * @param nom
	 * @return List<Enfant>
	 */
	List<Enfant> findByNomContaining(String nom);

}