package com.suivienfant.service;

import org.springframework.data.jpa.repository.JpaRepository;

import com.suivienfant.model.Mot;

/**
 * Service pour la récupération des mots
 */
public interface MotService extends JpaRepository<Mot, Long> {
}