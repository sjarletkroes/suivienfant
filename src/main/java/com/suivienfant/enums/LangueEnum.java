package com.suivienfant.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * Enum pour les langues des mots des enfants
 */
public enum LangueEnum {
	
	FRANCAIS("FRANCAIS"),
	
	NEERLANDAIS("NEERLANDAIS"),
	
	RUSSE("RUSSE"),
	
	ANGLAIS("ANGLAIS"),

	AUCUN("");

	private final String name;

	private static Map<String, LangueEnum> languesMap = new HashMap<String, LangueEnum>();

	static {
		languesMap.put("FRANCAIS", FRANCAIS);
		languesMap.put("NEERLANDAIS", NEERLANDAIS);
		languesMap.put("RUSSE", RUSSE);
		languesMap.put("ANGLAIS", ANGLAIS);
		languesMap.put("", AUCUN);
	}

	LangueEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
