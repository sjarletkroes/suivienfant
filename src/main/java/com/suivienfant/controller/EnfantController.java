package com.suivienfant.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.suivienfant.model.Enfant;
import com.suivienfant.service.EnfantService;

/**
 * Controller pour la gestion des mots
 */
@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api")
public class EnfantController {

  @Autowired
  EnfantService enfantService;

  /**
   * Récupère la liste des enfants, tous les enfants ou filtrés par nom
   * @param nom
   * @return List<Enfant>
   */
  @GetMapping("/enfants")
  public ResponseEntity<List<Enfant>> getAllEnfants(@RequestParam(required = false) String nom) {
    try {
      List<Enfant> enfants = new ArrayList<Enfant>();

      if (nom == null)
        enfantService.findAll().forEach(enfants::add);
      else
        enfantService.findByNomContaining(nom).forEach(enfants::add);

      if (enfants.isEmpty()) {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      }

      return new ResponseEntity<>(enfants, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Récupère l'enfant grâce à son id
   * @param id
   * @return Enfant
   */
  @GetMapping("/enfants/{id}")
  public ResponseEntity<Enfant> getEnfantById(@PathVariable("id") long id) {
    Optional<Enfant> enfantData = enfantService.findById(id);

    if (enfantData.isPresent()) {
      return new ResponseEntity<>(enfantData.get(), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  /**
   * Création d'un enfant
   * @param enfant
   * @return Enfant
   */
  @PostMapping("/enfants")
  public ResponseEntity<Enfant> createEnfant(@RequestBody Enfant enfant) {
    try {
      Enfant _enfant = enfantService
          .save(new Enfant(enfant.getNom(), enfant.getPrenom(), enfant.getDateNaissance()));
      return new ResponseEntity<>(_enfant, HttpStatus.CREATED);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Modification d'un enfant existant
   * @param id
   * @param enfant
   * @return Enfant
   */
  @PutMapping("/enfants/{id}")
  public ResponseEntity<Enfant> updateEnfant(@PathVariable("id") long id, @RequestBody Enfant enfant) {
    Optional<Enfant> enfantData = enfantService.findById(id);

    if (enfantData.isPresent()) {
      Enfant _enfant = enfantData.get();
      _enfant.setNom(enfant.getNom());
      _enfant.setPrenom(enfant.getPrenom());
      _enfant.setDateNaissance(enfant.getDateNaissance());
      return new ResponseEntity<>(enfantService.save(_enfant), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  /**
   * Suppression d'un enfant (à ne pas faire chez soi...)
   * @param id
   * @return HttpStatus
   */
  @DeleteMapping("/enfants/{id}")
  public ResponseEntity<HttpStatus> deleteEnfant(@PathVariable("id") long id) {
    try {
      enfantService.deleteById(id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Suppression de tous les enfants (bowling for columbine?)
   * @return HttpStatus
   */
  @DeleteMapping("/enfants")
  public ResponseEntity<HttpStatus> deleteAllEnfants() {
    try {
      enfantService.deleteAll();
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

  }

}