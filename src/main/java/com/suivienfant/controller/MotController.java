package com.suivienfant.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.suivienfant.model.Mot;
import com.suivienfant.service.MotService;

/**
 * Controller pour la gestion des mots
 */
@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api")
public class MotController {

  @Autowired
  MotService motService;

  /**
   * Récupère le mot grâce à son id
   * @param id
   * @return Mot
   */
  @GetMapping("/mots/{id}")
  public ResponseEntity<Mot> getMotById(@PathVariable("id") long id) {
    Optional<Mot> motData = motService.findById(id);

    if (motData.isPresent()) {
      return new ResponseEntity<>(motData.get(), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  /**
   * Création d'un mot
   * @param mot
   * @return Mot
   */
  @PostMapping("/mots")
  public ResponseEntity<Mot> createMot(@RequestBody Mot mot) {
    try {
      Mot _mot = motService
          .save(new Mot(mot.getLangue(), mot.getMot(), mot.getPrononciation(), mot.getDateAjout(), mot.getEnfant()));
      return new ResponseEntity<>(_mot, HttpStatus.CREATED);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Modification d'un mot existant
   * @param id
   * @param mot
   * @return Mot
   */
  @PutMapping("/mots/{id}")
  public ResponseEntity<Mot> updateMot(@PathVariable("id") long id, @RequestBody Mot mot) {
    Optional<Mot> motData = motService.findById(id);

    if (motData.isPresent()) {
      Mot _mot = motData.get();
      _mot.setLangue(mot.getLangue());
      _mot.setMot(mot.getMot());
      _mot.setPrononciation(mot.getPrononciation());
      _mot.setDateAjout(mot.getDateAjout());
      return new ResponseEntity<>(motService.save(_mot), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  /**
   * Suppression d'un mot
   * @param id
   * @return HttpStatus
   */
  @DeleteMapping("/mots/{id}")
  public ResponseEntity<HttpStatus> deleteMot(@PathVariable("id") long id) {
    try {
      motService.deleteById(id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}