package com.suivienfant.model;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Calendar;
import java.util.Date;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import com.suivienfant.enums.LangueEnum;
import com.suivienfant.service.MotService;

/**
 * Tests pour l'enregistrement / la modification / la suppression des mots
 */
@DataJpaTest
public class MotsTests {

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	MotService motService;

	@Test
	public void testNoMotsIfEmpty() {
	    Iterable<Mot> mots = motService.findAll();
	
	    assertThat(mots).isEmpty();
	}


	@Test
	void testSave() throws Exception {
		Date dateNaissance = new Date();
		Enfant enfant = new Enfant("Enfant1", "Prenom1", dateNaissance);
	    entityManager.persist(enfant);
		
		Date dateAjout = new Date();
		Mot mot = new Mot(LangueEnum.FRANCAIS, "Mot1", "Prononciation1", dateAjout, enfant);
	    this.entityManager.persist(mot);

	    assertThat(mot).hasFieldOrPropertyWithValue("langue", LangueEnum.FRANCAIS);
	    assertThat(mot).hasFieldOrPropertyWithValue("mot", "Mot1");
	    assertThat(mot).hasFieldOrPropertyWithValue("prononciation", "Prononciation1");
	    assertThat(mot).hasFieldOrPropertyWithValue("dateAjout", dateAjout);
	    assertThat(mot).hasFieldOrPropertyWithValue("enfant", enfant);
	}

	@Test
	public void testFindMotById() {
		Date dateNaissance = new Date();
		Enfant enfant = new Enfant("Enfant1", "Prenom1", dateNaissance);
	    entityManager.persist(enfant);
			
		Date dateAjout = new Date();
		
		Mot mot1 = new Mot(LangueEnum.FRANCAIS, "Mot1", "Prononciation1", dateAjout, enfant);
	    entityManager.persist(mot1);

	    Mot mot2 = new Mot(LangueEnum.FRANCAIS, "Mot2", "Prononciation2", dateAjout, enfant);
	    entityManager.persist(mot2);

	    Mot foundMot = motService.findById(mot2.getId()).get();

	    assertThat(foundMot).isEqualTo(mot2);
	}

	@Test
	public void testUpdateMotById() {
		Date dateNaissance = new Date();
		Enfant enfant = new Enfant("Enfant1", "Prenom1", dateNaissance);
	    entityManager.persist(enfant);
		
		Date dateAjout = new Date();
		
		Mot mot1 = new Mot(LangueEnum.FRANCAIS, "Mot1", "Prononciation1", dateAjout, enfant);
	    entityManager.persist(mot1);

	    Mot mot2 = new Mot(LangueEnum.FRANCAIS, "Mot2", "Prononciation2", dateAjout, enfant);
	    entityManager.persist(mot2);

	    Calendar c = Calendar.getInstance(); 
	    c.setTime(dateAjout); 
	    c.add(Calendar.DATE, 1);
	    Date updatedDateAjout = c.getTime();

	    Mot updatedMot = new Mot(LangueEnum.RUSSE, "updated Mot2", "updated Prononciation2", updatedDateAjout, enfant);

	    Mot mot = motService.findById(mot2.getId()).get();
	    mot.setLangue(updatedMot.getLangue());
	    mot.setMot(updatedMot.getMot());
	    mot.setPrononciation(updatedMot.getPrononciation());
	    mot.setDateAjout(updatedMot.getDateAjout());
	    motService.save(mot);

	    Mot checkMot = motService.findById(mot2.getId()).get();
	    
	    assertThat(checkMot.getId()).isEqualTo(mot2.getId());
	    assertThat(checkMot.getLangue()).isEqualTo(updatedMot.getLangue());
	    assertThat(checkMot.getMot()).isEqualTo(updatedMot.getMot());
	    assertThat(checkMot.getPrononciation()).isEqualTo(updatedMot.getPrononciation());
	    assertThat(checkMot.getDateAjout()).isEqualTo(updatedMot.getDateAjout());
	    assertThat(checkMot.getEnfant()).isEqualTo(updatedMot.getEnfant());
	}

	@Test
	public void testDeleteMotById() {
		Date dateNaissance = new Date();
		Enfant enfant = new Enfant("Enfant1", "Prenom1", dateNaissance);
	    entityManager.persist(enfant);
		
		Date dateAjout = new Date();
		
		Mot mot1 = new Mot(LangueEnum.FRANCAIS, "Mot1", "Prononciation1", dateAjout, enfant);
	    entityManager.persist(mot1);

	    Mot mot2 = new Mot(LangueEnum.FRANCAIS, "Mot2", "Prononciation2", dateAjout, enfant);
	    entityManager.persist(mot2);

	    Mot mot3 = new Mot(LangueEnum.FRANCAIS, "Mot3", "Prononciation3", dateAjout, enfant);
	    entityManager.persist(mot3);

	    motService.deleteById(mot2.getId());

	    Iterable<Mot> mots = motService.findAll();

	    assertThat(mots).hasSize(2).contains(mot1, mot3);
	}
}
