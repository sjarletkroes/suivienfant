package com.suivienfant.model;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Calendar;
import java.util.Date;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import com.suivienfant.service.EnfantService;

/**
 * Tests pour l'enregistrement / la modification / la suppression des enfants
 */
@DataJpaTest
public class EnfantsTests {

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	EnfantService enfantService;

	@Test
	public void testNoEnfantsIfEmpty() {
	    Iterable<Enfant> enfants = enfantService.findAll();
	
	    assertThat(enfants).isEmpty();
	}

	@Test
	void testSave() throws Exception {
		Date dateNaissance = new Date();
		Enfant enfant = new Enfant("Enfant1", "Prenom1", dateNaissance);
	    this.entityManager.persist(enfant);

	    assertThat(enfant).hasFieldOrPropertyWithValue("nom", "Enfant1");
	    assertThat(enfant).hasFieldOrPropertyWithValue("prenom", "Prenom1");
	    assertThat(enfant).hasFieldOrPropertyWithValue("dateNaissance", dateNaissance);
	}

	@Test
	public void testFindAll() {
		Date dateNaissance = new Date();
		
		Enfant enfant1 = new Enfant("Enfant1", "Prenom1", dateNaissance);
	    entityManager.persist(enfant1);

	    Enfant enfant2 = new Enfant("Enfant2", "Prenom2", dateNaissance);
	    entityManager.persist(enfant2);

	    Enfant enfant3 = new Enfant("Enfant3", "Prenom3", dateNaissance);
	    entityManager.persist(enfant3);

	    Iterable<Enfant> enfants = enfantService.findAll();

	    assertThat(enfants).hasSize(3).contains(enfant1, enfant2, enfant3);
	}

	  @Test
	  public void testFindEnfantById() {
		Date dateNaissance = new Date();
		
		Enfant enfant1 = new Enfant("Enfant1", "Prenom1", dateNaissance);
	    entityManager.persist(enfant1);

	    Enfant enfant2 = new Enfant("Enfant2", "Prenom2", dateNaissance);
	    entityManager.persist(enfant2);

	    Enfant foundEnfant = enfantService.findById(enfant2.getId()).get();

	    assertThat(foundEnfant).isEqualTo(enfant2);
	  }

	  @Test
	  public void testFindEnfantByNomContaining() {
		Date dateNaissance = new Date();
		
		Enfant enfant1 = new Enfant("Enfant1 findme", "Prenom1", dateNaissance);
	    entityManager.persist(enfant1);

	    Enfant enfant2 = new Enfant("Enf findme ant2", "Prenom2", dateNaissance);
	    entityManager.persist(enfant2);

	    Enfant enfant3 = new Enfant("Enfant3", "Prenom3", dateNaissance);
	    entityManager.persist(enfant3);

	    Iterable<Enfant> enfants = enfantService.findByNomContaining("findme");

	    assertThat(enfants).hasSize(2).contains(enfant1, enfant2);
	  }

	  @Test
	  public void testUpdateEnfantById() {
		Date dateNaissance = new Date();
		
		Enfant enfant1 = new Enfant("Enfant1", "Prenom1", dateNaissance);
	    entityManager.persist(enfant1);

	    Enfant enfant2 = new Enfant("Enfant2", "Prenom2", dateNaissance);
	    entityManager.persist(enfant2);

	    Calendar c = Calendar.getInstance(); 
	    c.setTime(dateNaissance); 
	    c.add(Calendar.DATE, 1);
	    Date updatedDateNaissance = c.getTime();

	    Enfant updatedEnfant = new Enfant("updated Enfant2", "updated Prenom2", updatedDateNaissance);

	    Enfant enfant = enfantService.findById(enfant2.getId()).get();
	    enfant.setNom(updatedEnfant.getNom());
	    enfant.setPrenom(updatedEnfant.getPrenom());
	    enfant.setDateNaissance(updatedEnfant.getDateNaissance());
	    enfantService.save(enfant);

	    Enfant checkEnfant = enfantService.findById(enfant2.getId()).get();
	    
	    assertThat(checkEnfant.getId()).isEqualTo(enfant2.getId());
	    assertThat(checkEnfant.getNom()).isEqualTo(updatedEnfant.getNom());
	    assertThat(checkEnfant.getPrenom()).isEqualTo(updatedEnfant.getPrenom());
	    assertThat(checkEnfant.getDateNaissance()).isEqualTo(updatedEnfant.getDateNaissance());
	  }

	  @Test
	  public void testDeleteEnfantById() {
		Date dateNaissance = new Date();
		
		Enfant enfant1 = new Enfant("Enfant1", "Prenom1", dateNaissance);
	    entityManager.persist(enfant1);

	    Enfant enfant2 = new Enfant("Enfant2", "Prenom2", dateNaissance);
	    entityManager.persist(enfant2);

	    Enfant enfant3 = new Enfant("Enfant3", "Prenom3", dateNaissance);
	    entityManager.persist(enfant3);

	    enfantService.deleteById(enfant2.getId());

	    Iterable<Enfant> enfants = enfantService.findAll();

	    assertThat(enfants).hasSize(2).contains(enfant1, enfant3);
	  }

	  @Test
	  public void testDeleteAllEnfants() {
			Date dateNaissance = new Date();
			
	    entityManager.persist(new Enfant("Enfant1", "Prenom1", dateNaissance));
	    entityManager.persist(new Enfant("Enfant2", "Prenom2", dateNaissance));

	    enfantService.deleteAll();

	    assertThat(enfantService.findAll()).isEmpty();
	  }
}
