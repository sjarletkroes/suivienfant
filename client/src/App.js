import React, { Component } from "react";
import { Switch, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import './App.css';

import AddEnfant from "./components/enfant_add";
import Enfant from "./components/enfant";
import EnfantsList from "./components/enfant_list";
import AddMot from "./components/mot_add";
import Mot from "./components/mot";

class App extends Component {
  render() {
    return (
      <div>
        <nav className="navbar navbar-expand navbar-dark bg-dark">
          <a href="/enfants" className="navbar-brand">
            Suivi de l'enfant et son vocabulaire
          </a>
          <div className="navbar-nav mr-auto">
            <li className="nav-item">
              <Link to={"/enfants"} className="nav-link">
                Enfants
              </Link>
            </li>
            <li className="nav-item">
              <Link to={"/add"} className="nav-link">
                Ajouter
              </Link>
            </li>
          </div>
        </nav>

        <div className="container mt-3">
          <Switch>
            <Route exact path={["/", "/enfants"]} component={EnfantsList} />
            <Route exact path="/add" component={AddEnfant} />
            <Route path="/enfants/:id" component={Enfant} />
            <Route path="/addmot/:id" component={AddMot} />
            <Route path="/mots/:id" component={Mot} />
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;
