import React, { Component } from "react";
import MotDataService from "../services/motService";
import ReactDatePicker from "react-datepicker";
import { parseISO } from "date-fns";
import moment from 'moment';
import "react-datepicker/dist/react-datepicker.css";
import { registerLocale } from "react-datepicker";
import fr from 'date-fns/locale/fr';

registerLocale('fr', fr)

export default class Mot extends Component {
    constructor(props) {
        super(props);
        this.onChangeLangue = this.onChangeLangue.bind(this);
        this.onChangeMot = this.onChangeMot.bind(this);
        this.onChangePrononciation = this.onChangePrononciation.bind(this);
        this.onChangeDateAjout = this.onChangeDateAjout.bind(this);
        this.getMot = this.getMot.bind(this);
        this.updateMot = this.updateMot.bind(this);
        this.deleteMot = this.deleteMot.bind(this);
        this.formatDate = this.formatDate.bind(this);

        this.state = {
            currentMot: {
                id: null,
                langue: "",
                mot: "",
                prononciation: "",
                dateAjout: "",
                enfant: {
                  id: ""
                }
            },
            message: ""
        }
    }

    componentDidMount() {
        this.getMot(this.props.match.params.id);
    }

    onChangeLangue(e) {
        const langue = e.target.value;

        this.setState(prevState => ({
            currentMot: {
                ...prevState.currentMot,
                langue: langue
            }
        }));
    }

    onChangeMot(e) {
        const mot = e.target.value;

        this.setState(prevState => ({
            currentMot: {
                ...prevState.currentMot,
                mot: mot
            }
        }));
    }

    onChangePrononciation(e) {
        const prononciation = e.target.value;

        this.setState(prevState => ({
            currentMot: {
                ...prevState.currentMot,
                prononciation: prononciation
            }
        }));
    }

    onChangeDateAjout(e) {
        const dateAjout = moment(e).format('YYYY-MM-DD');

        this.setState(prevState => ({
            currentMot: {
                ...prevState.currentMot,
                dateAjout: dateAjout
            }
        }));
    }

    getMot(id) {
        MotDataService.get(id)
            .then(response => {
                this.setState({
                    currentMot: response.data
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    updateMot() {
        MotDataService.update(
            this.state.currentMot.id,
            this.state.currentMot
        )
            .then(response => {
                this.setState({
                    message: "Mot mis à jour avec succès!"
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    deleteMot() {
        MotDataService.delete(this.state.currentMot.id)
            .then(response => {
                this.props.history.push('/enfants/')
            })
            .catch(e => {
                console.log(e);
            });
    }

    /**
     * Formatage de la date
     * @param {*} date 
     */
    formatDate(date) {
        return moment(date).format("DD/MM/yyyy");
    }

    render() {
        const { currentMot } = this.state;

        return (
            <div>
                {currentMot ? (
                    <div className="edit-form">
                        <h4>Mot</h4>
                        <form>
                            {/* Langue */}
                            <div className="form-group">
                                <label htmlFor="langue">Langue</label>
                                <select
                                    value={this.state.langue}
                                    onChange={this.onChangeLangue}
                                    id="langue"
                                    // value={currentMot.langue}
                                    className="form-control"
                                >
                                    <option value="FRANCAIS">Français</option>
                                    <option value="NEERLANDAIS">Néerlandais</option>
                                    <option value="RUSSE">Russe</option>
                                    <option value="ANGLAIS">Anglais</option>
                                </select>
                            </div>
                            {/* mot */}
                            <div className="form-group">
                                <label htmlFor="mot">Mot</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="mot"
                                    value={currentMot.mot}
                                    onChange={this.onChangeMot}
                                />
                            </div>
                            {/* prononciation */}
                            <div className="form-group">
                                <label htmlFor="prononciation">Prononciation</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="prononciation"
                                    value={currentMot.prononciation}
                                    onChange={this.onChangePrononciation}
                                />
                            </div>
                            {/* Date d'ajout' */}
                            <div className="form-group">
                                <label htmlFor="dateAjout">Date d'ajout</label>
                                <br></br>
                                <ReactDatePicker
                                    locale="fr"
                                    className="form-control"
                                    id="dateAjout"
                                    dateFormat="dd/MM/yyyy"
                                    selected={currentMot.dateAjout !== null && currentMot.dateAjout !== "" ? 
                                        parseISO(currentMot.dateAjout) :
                                        null}
                                    onChange={this.onChangeDateAjout}
                                />
                            </div>
                        </form>

                        <button
                            className="badge badge-danger mr-2"
                            onClick={this.deleteMot}
                        >
                            Supprimer
                        </button>

                        <button
                            type="submit"
                            className="badge badge-success"
                            onClick={this.updateMot}
                        >
                            Mettre à jour
                        </button>
                        <p>{this.state.message}</p>
                    </div>
                ) : (
                        <div>
                            <br />
                            <p>Sélectionner un mot...</p>
                        </div>
                    )}
            </div>
        );
    }
}