import React, { Component } from "react";
import MotDataService from "../services/motService";
import ReactDatePicker from "react-datepicker";
import moment from 'moment';
import "react-datepicker/dist/react-datepicker.css";
import { registerLocale } from "react-datepicker";
import fr from 'date-fns/locale/fr';
registerLocale('fr', fr)

export default class AddMot extends Component {
  constructor(props) {
    super(props);
    this.onChangeLangue = this.onChangeLangue.bind(this);
    this.onChangeMot = this.onChangeMot.bind(this);
    this.onChangePrononciation = this.onChangePrononciation.bind(this);
    this.onChangeDateAjout = this.onChangeDateAjout.bind(this);
    this.saveMot = this.saveMot.bind(this);
    this.newMot = this.newMot.bind(this);

    this.state = {
      id: null,
      langue: "FRANCAIS",
      mot: "",
      prononciation: "",
      dateAjout: "",
      enfant: {
        id: this.props.match.params.id
      },

      submitted: false
    };
  }

  componentDidMount() {
    this.setState({
      enfant: {
        id: this.props.match.params.id
      }
    });
  }

  onChangeLangue(e) {
    this.setState({
      langue: e.target.value
    });
  }

  /**
   * Modifier le mot
   * @param {*} e 
   */
  onChangeMot(e) {
    this.setState({
      mot: e.target.value
    });
  }

  /**
   * Modifier le prémot
   * @param {*} e 
   */
  onChangePrononciation(e) {
    this.setState({
      prononciation: e.target.value
    });
  }

  /**
   * Modifier la date de naissance
   * @param {*} e 
   */
  onChangeDateAjout(e) {
    this.setState({
      dateAjout: moment(e).format("DD/MM/yyyy")
    });
  }

  /**
   * Enregistrer l'mot
   */
  saveMot() {
    var data = {
      langue: this.state.langue,
      mot: this.state.mot,
      prononciation: this.state.prononciation,
      dateAjout: moment(this.state.dateAjout, "DD/MM/yyyy"),
      enfant: {
        id: this.props.match.params.id
      }
    };

    MotDataService.create(data)
      .then(response => {
        this.setState({
          id: response.data.id,
          langue: response.data.langue,
          mot: response.data.mot,
          prononciation: response.data.prononciation,
          dateAjout: response.data.dateAjout,

          submitted: true
        });
      })
      .catch(e => {
        console.log(e);
      });
  }

  /**
   * Créer un nouvel mot
   */
  newMot() {
    this.setState({
      id: null,
      langue: "FRANCAIS",
      mot: "",
      prononciation: "",
      dateAjout: "",
      enfant: {
        id: this.props.match.params.id
      },

      submitted: false
    });
  }

  render() {
    return (
      <div className="submit-form">
        {this.state.submitted ? (
          <div>
            <h4>Enregistrement réussi!</h4>
            <button className="btn btn-success" onClick={this.newMot}>
              Ajouter
            </button>
          </div>
        ) : (
            <div>
              {/* Langue */}
              <div className="form-group">
                <label htmlFor="langue">Langue</label>
                <select
                  value={this.state.langue}
                  onChange={this.onChangeLangue}
                  id="langue"
                  // value={currentMot.langue}
                  className="form-control"
                >
                  <option value="FRANCAIS">Français</option>
                  <option value="NEERLANDAIS">Néerlandais</option>
                  <option value="RUSSE">Russe</option>
                  <option value="ANGLAIS">Anglais</option>
                </select>
              </div>
              {/* Mot */}
              <div className="form-group">
                <label htmlFor="mot">Mot</label>
                <input
                  type="text"
                  className="form-control"
                  id="mot"
                  required
                  value={this.state.mot}
                  onChange={this.onChangeMot}
                  name="mot"
                />
              </div>

              {/* Prononciation */}
              <div className="form-group">
                <label htmlFor="prononciation">Prononciation</label>
                <input
                  type="text"
                  className="form-control"
                  id="prononciation"
                  value={this.state.prononciation}
                  onChange={this.onChangePrononciation}
                  name="prononciation"
                />
              </div>

              {/* Date de naissance */}
              <div className="form-group">
                <label htmlFor="dateAjout">Date d'ajout</label>
                <br></br>
                <ReactDatePicker
                  locale="fr"
                  className="form-control"
                  id="dateAjout"
                  name="dateAjout"
                  value={this.state.dateAjout}
                  dateFormat="dd/MM/yyyy"
                  // selected={parseISO(this.state.dateAjout)}
                  onChange={this.onChangeDateAjout}
                />
              </div>

              <button onClick={this.saveMot} className="btn btn-success">
                Valider
            </button>
            </div>
          )}
      </div>
    );
  }
}