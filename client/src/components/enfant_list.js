import React, { Component } from "react";
import EnfantDataService from "../services/enfantService";
import { Link } from "react-router-dom";
import moment from 'moment';

export default class EnfantsList extends Component {
  constructor(props) {
    super(props);
    this.onChangeSearchNom = this.onChangeSearchNom.bind(this);
    this.retrieveEnfants = this.retrieveEnfants.bind(this);
    this.refreshList = this.refreshList.bind(this);
    this.setActiveEnfant = this.setActiveEnfant.bind(this);
    this.removeAllEnfants = this.removeAllEnfants.bind(this);
    this.searchNom = this.searchNom.bind(this);
    this.formatNomComplet = this.formatNomComplet.bind(this);
    this.formatDate = this.formatDate.bind(this);
    this.formatMot = this.formatMot.bind(this);

    this.state = {
      enfants: [],
      currentEnfant: null,
      currentIndex: -1,
      searchNom: ""
    };
  }

  componentDidMount() {
    this.retrieveEnfants();
  }

  /**
   * Modification recherche par nom
   * @param {*} e 
   */
  onChangeSearchNom(e) {
    const searchNom = e.target.value;

    this.setState({
      searchNom: searchNom
    });
  }

  retrieveEnfants() {
    EnfantDataService.getAll()
      .then(response => {
        this.setState({
          enfants: response.data
        });
      })
      .catch(e => {
        console.log(e);
      });
  }

  /**
   * Remise à zéro de la liste (nouvelle requete, aucune sélection)
   */
  refreshList() {
    this.retrieveEnfants();
    this.setState({
      currentEnfant: null,
      currentIndex: -1
    });
  }

  /**
   * Sélection d'un enfant
   * @param {*} enfant 
   * @param {*} index 
   */
  setActiveEnfant(enfant, index) {
    this.setState({
      currentEnfant: enfant,
      currentIndex: index
    });
  }

  /**
   * Supprimer tous les enfants
   */
  removeAllEnfants() {
    EnfantDataService.deleteAll()
      .then(response => {
        this.refreshList();
      })
      .catch(e => {
        console.log(e);
      });
  }

  /**
   * Recherche par nom 
   */
  searchNom() {
    EnfantDataService.findByNom(this.state.searchNom)
      .then(response => {
        this.setState({
          enfants: response.data
        });
      })
      .catch(e => {
        console.log(e);
      });
  }

  /**
   * Formatage du nom complet (nom + prénom) de l'enfant
   * @param {*} enfant 
   */
  formatNomComplet(enfant) {
    return enfant.nom + " " + enfant.prenom;
  }

  /**
   * Formatage de la date
   * @param {*} date 
   */
  formatDate(date) {
    return moment(date).format("DD/MM/yyyy");
  }

  formatMot(index, mot) {
    return (
      <tr key={"mot" + index}>
        <td>{mot.langue}</td>
        <td>{mot.mot}</td>
        <td>{mot.prononciation}</td>
        <td>{this.formatDate(mot.dateAjout)}</td>
      </tr>
    );
  }

  render() {
    const { searchNom, enfants, currentEnfant, currentIndex } = this.state;

    return (
      <div className="list row">
        {/* Recherche */}
        <div className="col-md-8">
          <div className="input-group mb-3">
            <input
              type="text"
              className="form-control"
              placeholder="Recherche par nom"
              value={searchNom}
              onChange={this.onChangeSearchNom}
            />
            <div className="input-group-append">
              <button
                className="btn btn-outline-secondary"
                type="button"
                onClick={this.searchNom}
              >
                Rechercher
              </button>
            </div>
          </div>
        </div>
        {/* Résultat */}
        <div className="col-md-6">
          <h4>Liste des enfants</h4>

          <ul className="list-group">
            {enfants &&
              enfants.map((enfant, index) => (
                <li
                  className={
                    "list-group-item " +
                    (index === currentIndex ? "active" : "")
                  }
                  onClick={() => this.setActiveEnfant(enfant, index)}
                  key={"enfant" + index}
                >
                  {this.formatNomComplet(enfant)}
                </li>
              ))}
          </ul>

          <button
            className="m-3 btn btn-sm btn-danger"
            onClick={this.removeAllEnfants}
          >
            Supprimer tous les enfants
          </button>
        </div>
        <div className="col-md-6">
          {currentEnfant ? (
            <div>
              <h4>Enfant</h4>
              <div>
                <label>
                  <strong>Nom:</strong>
                </label>{" "}
                {currentEnfant.nom}
              </div>
              <div>
                <label>
                  <strong>Prenom:</strong>
                </label>{" "}
                {currentEnfant.prenom}
              </div>
              <div>
                <label>
                  <strong>Date de naissance:</strong>
                </label>{" "}
                {this.formatDate(currentEnfant.dateNaissance)}
              </div>
              <div>
                <h4>Liste mots/phrases:</h4>

                <table>
                  <thead>
                    <tr>
                      <th>Langue</th>
                      <th>Mot</th>
                      <th>Prononciation</th>
                      <th>Date ajout</th>
                    </tr>
                  </thead>
                  <tbody>
                    {currentEnfant.listeMots &&
                      currentEnfant.listeMots.map((mot, index) => (
                        this.formatMot(index, mot)
                      ))}
                  </tbody>
                </table>
              </div>

              <Link
                to={"/enfants/" + currentEnfant.id}
                className="badge badge-warning"
              >
                Editer
              </Link>
            </div>
          ) : (
              <div>
                <br />
                <p>Sélectionner un enfant...</p>
              </div>
            )}
        </div>
      </div>
    );
  }
}