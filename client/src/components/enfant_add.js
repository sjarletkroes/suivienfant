import React, { Component } from "react";
import EnfantDataService from "../services/enfantService";
import ReactDatePicker from "react-datepicker";
import moment from 'moment';
import "react-datepicker/dist/react-datepicker.css";
import { registerLocale } from "react-datepicker";
import fr from 'date-fns/locale/fr';
registerLocale('fr', fr)

export default class AddEnfant extends Component {
  constructor(props) {
    super(props);
    this.onChangeNom = this.onChangeNom.bind(this);
    this.onChangePrenom = this.onChangePrenom.bind(this);
    this.onChangeDateNaissance = this.onChangeDateNaissance.bind(this);
    this.saveEnfant = this.saveEnfant.bind(this);
    this.newEnfant = this.newEnfant.bind(this);

    this.state = {
      id: null,
      nom: "",
      prenom: "",
      dateNaissance: "",

      submitted: false
    };
  }

  /**
   * Modifier le nom
   * @param {*} e 
   */
  onChangeNom(e) {
    this.setState({
      nom: e.target.value
    });
  }

  /**
   * Modifier le prénom
   * @param {*} e 
   */
  onChangePrenom(e) {
    this.setState({
      prenom: e.target.value
    });
  }

  /**
   * Modifier la date d'ajout
   * @param {*} e 
   */
  onChangeDateNaissance(e) {
    this.setState({
      dateNaissance: moment(e).format("DD/MM/yyyy")
    });
  }

  /**
   * Enregistrer l'enfant
   */
  saveEnfant() {
    var data = {
      nom: this.state.nom,
      prenom: this.state.prenom,
      dateNaissance: moment(this.state.dateNaissance, "DD/MM/yyyy")
    };

    EnfantDataService.create(data)
      .then(response => {
        this.setState({
          id: response.data.id,
          nom: response.data.nom,
          prenom: response.data.prenom,
          dateNaissance: response.data.dateNaissance,

          submitted: true
        });
      })
      .catch(e => {
        console.log(e);
      });
  }

  /**
   * Créer un nouvel enfant
   */
  newEnfant() {
    this.setState({
      id: null,
      nom: "",
      prenom: "",
      dateNaissance: "",

      submitted: false
    });
  }

  render() {
    return (
      <div className="submit-form">
        {this.state.submitted ? (
          <div>
            <h4>Enregistrement réussi!</h4>
            <button className="btn btn-success" onClick={this.newEnfant}>
              Ajouter
            </button>
          </div>
        ) : (
            <div>
              {/* Nom */}
              <div className="form-group">
                <label htmlFor="nom">Nom</label>
                <input
                  type="text"
                  className="form-control"
                  id="nom"
                  required
                  value={this.state.nom}
                  onChange={this.onChangeNom}
                  name="nom"
                />
              </div>

              {/* Prénom */}
              <div className="form-group">
                <label htmlFor="prenom">Prenom</label>
                <input
                  type="text"
                  className="form-control"
                  id="prenom"
                  value={this.state.prenom}
                  onChange={this.onChangePrenom}
                  name="prenom"
                />
              </div>

              {/* Date de naissance */}
              <div className="form-group">
                <label htmlFor="dateNaissance">Date de naissance</label>
                <br></br>
                <ReactDatePicker
                  locale="fr"
                  className="form-control"
                  id="dateNaissance"
                  name="dateNaissance"
                  value={this.state.dateNaissance}
                  dateFormat="dd/MM/yyyy"
                  // selected={parseISO(this.state.dateNaissance)}
                  onChange={this.onChangeDateNaissance}
                />
              </div>

              <button onClick={this.saveEnfant} className="btn btn-success">
                Valider
            </button>
            </div>
          )}
      </div>
    );
  }
}