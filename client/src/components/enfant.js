import React, { Component } from "react";
import EnfantDataService from "../services/enfantService";
import { Link } from "react-router-dom";
import ReactDatePicker from "react-datepicker";
import { parseISO } from "date-fns";
import moment from 'moment';
import "react-datepicker/dist/react-datepicker.css";
import { registerLocale } from "react-datepicker";
import fr from 'date-fns/locale/fr';

registerLocale('fr', fr)

export default class Enfant extends Component {
    constructor(props) {
        super(props);
        this.onChangeNom = this.onChangeNom.bind(this);
        this.onChangePrenom = this.onChangePrenom.bind(this);
        this.onChangeDateNaissance = this.onChangeDateNaissance.bind(this);
        this.getEnfant = this.getEnfant.bind(this);
        this.updateEnfant = this.updateEnfant.bind(this);
        this.deleteEnfant = this.deleteEnfant.bind(this);
        this.formatDate = this.formatDate.bind(this);
        this.formatMot = this.formatMot.bind(this);

        this.state = {
            currentEnfant: {
                id: null,
                nom: "",
                prenom: "",
                dateNaissance: ""
            },
            message: ""
        };
    }

    componentDidMount() {
        this.getEnfant(this.props.match.params.id);
    }

    onChangeNom(e) {
        const nom = e.target.value;

        this.setState(function (prevState) {
            return {
                currentEnfant: {
                    ...prevState.currentEnfant,
                    nom: nom
                }
            };
        });
    }

    onChangePrenom(e) {
        const prenom = e.target.value;

        this.setState(prevState => ({
            currentEnfant: {
                ...prevState.currentEnfant,
                prenom: prenom
            }
        }));
    }

    onChangeDateNaissance(e) {
        const dateNaissance = moment(e).format('YYYY-MM-DD');

        this.setState(prevState => ({
            currentEnfant: {
                ...prevState.currentEnfant,
                dateNaissance: dateNaissance
            }
        }));
    }

    getEnfant(id) {
        EnfantDataService.get(id)
            .then(response => {
                this.setState({
                    currentEnfant: response.data
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    updateEnfant() {
        EnfantDataService.update(
            this.state.currentEnfant.id,
            this.state.currentEnfant
        )
            .then(response => {
                this.setState({
                    message: "Enfant mis à jour avec succès!"
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    deleteEnfant() {
        EnfantDataService.delete(this.state.currentEnfant.id)
            .then(response => {
                this.props.history.push('/enfants')
            })
            .catch(e => {
                console.log(e);
            });
    }

    /**
     * Formatage de la date
     * @param {*} date 
     */
    formatDate(date) {
        return moment(date).format("DD/MM/yyyy");
    }

    formatMot(index, mot) {
        return (
            <tr key={"mot" + index}>
                <td>{mot.langue}</td>
                <td>{mot.mot}</td>
                <td>{mot.prononciation}</td>
                <td>{this.formatDate(mot.dateAjout)}</td>
                <td>
                    <Link
                        to={"/mots/" + mot.id}
                        className="badge badge-warning"
                    >
                        Editer
                    </Link>
                </td>
            </tr>
        );
    }

    render() {
        const { currentEnfant } = this.state;

        return (
            <div>
                {currentEnfant ? (
                    <div className="edit-form">
                        <h4>Enfant</h4>
                        <form>
                            {/* Nom */}
                            <div className="form-group">
                                <label htmlFor="nom">Nom</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="nom"
                                    value={currentEnfant.nom}
                                    onChange={this.onChangeNom}
                                />
                            </div>
                            {/* Prénom */}
                            <div className="form-group">
                                <label htmlFor="prenom">Prenom</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="prenom"
                                    value={currentEnfant.prenom}
                                    onChange={this.onChangePrenom}
                                />
                            </div>
                            {/* Date de naissance */}
                            <div className="form-group">
                                <label htmlFor="dateNaissance">Date de naissance</label>
                                <br></br>
                                <ReactDatePicker
                                    locale="fr"
                                    className="form-control"
                                    id="dateNaissance"
                                    dateFormat="dd/MM/yyyy"
                                    selected={currentEnfant.dateNaissance !== null && currentEnfant.dateNaissance !== "" ? 
                                        parseISO(currentEnfant.dateNaissance) :
                                        null}
                                    onChange={this.onChangeDateNaissance}
                                />
                            </div>

                        </form>

                        <button
                            className="badge badge-danger mr-2"
                            onClick={this.deleteEnfant}
                        >
                            Supprimer
                        </button>

                        <button
                            type="submit"
                            className="badge badge-success"
                            onClick={this.updateEnfant}
                        >
                            Mettre à jour
                        </button>
                        <p>{this.state.message}</p>

                        <div>
                            <h4>Liste mots/phrases:</h4>
                            
                            <Link to={"/addmot/" + currentEnfant.id} className="badge badge-primary">
                                Ajouter un mot
                            </Link>
                            <table>
                                <thead>
                                    <tr>
                                        <th>Langue</th>
                                        <th>Mot</th>
                                        <th>Prononciation</th>
                                        <th>Date ajout</th>
                                        <th>Editer</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {currentEnfant.listeMots &&
                                        currentEnfant.listeMots.map((mot, index) => (
                                            this.formatMot(index, mot)
                                        ))}
                                </tbody>
                            </table>
                        </div>
                    </div>
                ) : (
                        <div>
                            <br />
                            <p>Sélectionner un enfant...</p>
                        </div>
                    )}
            </div>
        );
    }
}