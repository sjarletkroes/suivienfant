import http from "../http-common";

class EnfantDataService {
  getAll() {
    return http.get("/enfants");
  }

  get(id) {
    return http.get(`/enfants/${id}`);
  }

  create(data) {
    return http.post("/enfants", data);
  }

  update(id, data) {
    return http.put(`/enfants/${id}`, data);
  }

  delete(id) {
    return http.delete(`/enfants/${id}`);
  }

  deleteAll() {
    return http.delete(`/enfants`);
  }

  findByNom(nom) {
    return http.get(`/enfants?nom=${nom}`);
  }
}

export default new EnfantDataService();