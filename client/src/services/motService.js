import http from "../http-common";

class MotDataService {
  get(id) {
    return http.get(`/mots/${id}`);
  }

  create(data) {
    return http.post("/mots", data);
  }

  update(id, data) {
    return http.put(`/mots/${id}`, data);
  }

  delete(id) {
    return http.delete(`/mots/${id}`);
  }
}

export default new MotDataService();