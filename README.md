# Suivi de l'enfant et son vocabulaire

Application permettant d'enregistrer des enfants (nom, prénom, date de naissance) et de leur associer des mots (langue, mot, prononciation, date d'ajout au vocabulaire de l'enfant).

On peut imaginer dans un second temps d'agrandir les données et de pouvoir avoir des familles ayant des enfants. Ajouter des graphiques d'évolution du vocabulaire dans le temps ou encore pouvoir associer les prononciations successives à des dates.

## Système

* BDD: PostgreSQL
* Backend: Java
* Frontend: React

## Détail

### PostgreSQL

#### Installé:

* PostgreSQL 13

#### src/main/resources/application.properties
````
spring.datasource.url= jdbc:postgresql://localhost:5432/suivienfantdb
spring.datasource.username= postgres
spring.datasource.password= admin
````

### Java

#### Installé:

* Java 11
* Maven
* Hibernate
* JUnit

#### Pour démarrer: 

``mvn spring-boot:run``

#### Détail des fichiers

* com.suivienfant.model: contient les objets Enfant et Mot
* com.suivienfant.controller: contient les controllers qui contiennent les méthodes pour les requêtes RESTful
* com.suivienfant.service: contient les interfaces qui étendent JpaRepository pour les méthodes CRUD
* com.suivienfant.enums: package pour les types énumérés

### React

#### Installé:

* NodeJs 12

#### Pour démarrer l'application: 

``npm start``

Url: [http://localhost:8081/](http://localhost:8081/)

#### Détail des fichiers

* package.json modules principaux: react, react-router-dom, axios & bootstrap.
* App contient Router & navbar.
* 5 composants: EnfantList, Enfant, AddEnfant & Mot, AddMot.
* http-common.js initialise axios avec HTTP base Url et headers.
* EnfantDataService et MotDataService contiennent les méthodes pour envoyer les requêtes HTTP à l'API.
* .env configure le port pour l'application React CRUD.

## Images

### Liste des enfants

![](images/Liste_enfants.PNG)

### Modification enfant

![](images/Modifier_enfant.PNG)

### Modification mot

![](images/Modifier_mot.PNG)
